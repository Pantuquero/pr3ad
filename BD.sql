CREATE DATABASE IF NOT EXISTS pr3AD;

USE pr3AD;

CREATE TABLE IF NOT EXISTS users(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	mail VARCHAR(100) NOT NULL,
	nick VARCHAR(50) NOT NULL,
	pass VARCHAR(50)
);

INSERT INTO users (name, mail, nick, pass) VALUES ('root', 'user@mail.com', 'nick', 'pass');

CREATE TABLE IF NOT EXISTS customers(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	surname VARCHAR(50),
	birth DATE,
	adress VARCHAR(100) NOT NULL,
	mail VARCHAR(100) NOT NULL,
	telephone VARCHAR(12)
);

CREATE TABLE IF NOT EXISTS manufacturers(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	code VARCHAR(12) NOT NULL,
	adress VARCHAR(100) NOT NULL,
	mail VARCHAR(100) NOT NULL,
	telephone VARCHAR(12) NOT NULL
);

CREATE TABLE IF NOT EXISTS workers(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	dni VARCHAR(9) NOT NULL,
	name VARCHAR(50) NOT NULL,
	surname VARCHAR(50) NOT NULL,
	bank_account VARCHAR(50),
	manufacturer_id INT UNSIGNED,
	FOREIGN KEY(manufacturer_id) REFERENCES manufacturers(id) ON DELETE set null ON UPDATE cascade
);

CREATE TABLE IF NOT EXISTS deliverers(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	code VARCHAR(12) NOT NULL,
	adress VARCHAR(100) NOT NULL,
	mail VARCHAR(100) NOT NULL,
	telephone VARCHAR(12) NOT NULL
);

CREATE TABLE IF NOT EXISTS computers(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	code VARCHAR(12) NOT NULL,
	name VARCHAR(100) NOT NULL,
	description VARCHAR(500),
	price FLOAT,

	customer_id INT UNSIGNED,
	manufacturer_id INT UNSIGNED,
	FOREIGN KEY(customer_id) REFERENCES customers(id) ON DELETE set null ON UPDATE cascade,
	FOREIGN KEY(manufacturer_id) REFERENCES manufacturers(id) ON DELETE set null ON UPDATE cascade
);

CREATE TABLE IF NOT EXISTS manufacturers_deliverers(
	id_manufacturer INT UNSIGNED,
	id_deliverer INT UNSIGNED,
	FOREIGN KEY (id_manufacturer) REFERENCES manufacturers(id) ON DELETE cascade ON UPDATE cascade,
	FOREIGN KEY (id_deliverer) REFERENCES deliverers(id) ON DELETE cascade ON UPDATE cascade
);