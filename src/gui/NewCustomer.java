package gui;

import com.toedter.calendar.JDateChooser;
import data.Customer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NewCustomer extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfName;
    private JTextField tfSurname;
    private JTextField tfAdress;
    private JTextField tfMail;
    private JTextField tfTelephone;
    private JDateChooser dtBirth;

    private Customer customer;

    public NewCustomer() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newCustomer();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    public void newCustomer(){
        if(!tfName.getText().equalsIgnoreCase("") || !tfAdress.getText().equalsIgnoreCase("") || !tfMail.getText().equalsIgnoreCase("")){
            this.customer = new Customer();

            this.customer.setName(tfName.getText());
            this.customer.setSurname(tfSurname.getText());
            this.customer.setBirth(new java.sql.Date(dtBirth.getDate().getTime()));
            this.customer.setAdress(tfAdress.getText());
            this.customer.setMail(tfMail.getText());
            this.customer.setTelephone(tfTelephone.getText());

            setVisible(false);
        }else{
            JOptionPane.showMessageDialog(this, "You must fill in the obligatory fields");
        }
    }

    public Customer getCustomer(){
        return this.customer;
    }

    public static void main(String[] args) {
        NewCustomer dialog = new NewCustomer();
        dialog.setTitle("New customer");
        dialog.setDefaultCloseOperation(HIDE_ON_CLOSE);
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
