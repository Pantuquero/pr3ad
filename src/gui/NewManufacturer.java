package gui;

import data.Deliverer;
import data.Manufacturer;
import org.hibernate.Query;
import util.HibernateUtil;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class NewManufacturer extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfName;
    private JTextField tfCode;
    private JTextField tfAdress;
    private JTextField tfMail;
    private JTextField tfTelephone;
    private JScrollPane scrollDeliverers;
    private JList lstDeliverers;

    private Manufacturer manufacturer;
    private DefaultListModel delivererListModel;
    private List<Deliverer> delivererList;

    public NewManufacturer() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        loadDeliverersList();

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newManufacturer();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    private void newManufacturer(){
        if(!tfName.getText().equalsIgnoreCase("") || !tfCode.getText().equalsIgnoreCase("") || !tfAdress.getText().equalsIgnoreCase("") || !tfMail.getText().equalsIgnoreCase("") || !tfTelephone.getText().equalsIgnoreCase("")){
            if(this.manufacturer == null)
                this.manufacturer = new Manufacturer();

            this.manufacturer.setName(tfName.getText());
            this.manufacturer.setCode(tfCode.getText());
            this.manufacturer.setAdress(tfAdress.getText());
            this.manufacturer.setMail(tfMail.getText());
            this.manufacturer.setTelephone(tfTelephone.getText());
            this.manufacturer.setDeliverers(lstDeliverers.getSelectedValuesList());

            setVisible(false);
        }else{
            JOptionPane.showMessageDialog(this, "You must fill in the obligatory fields");
        }
    }

    public void prepareEdit(int id_manufacturer){
        buttonOK.setText("Edit");
        this.manufacturer = (Manufacturer) HibernateUtil.getCurrentSession().get(Manufacturer.class, id_manufacturer);

        tfName.setText(manufacturer.getName());
        tfCode.setText(manufacturer.getCode());
        tfAdress.setText(manufacturer.getAdress());
        tfMail.setText(manufacturer.getMail());
        tfTelephone.setText(manufacturer.getTelephone());
    }

    private void loadDeliverersList(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Deliverer");
        this.delivererList = query.list();

        this.delivererListModel = new DefaultListModel();
        this.delivererListModel.removeAllElements();
        for(Deliverer deliverer : delivererList)
            this.delivererListModel.addElement(deliverer);

        this.lstDeliverers.setModel(delivererListModel);
    }

    public Manufacturer getManufacturer() {
        return this.manufacturer;
    }

    public static void main(String[] args) {
        NewManufacturer dialog = new NewManufacturer();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
