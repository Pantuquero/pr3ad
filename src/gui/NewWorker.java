package gui;

import data.Manufacturer;
import data.Worker;
import org.hibernate.Query;
import org.hibernate.Session;
import util.HibernateUtil;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class NewWorker extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfDni;
    private JTextField tfName;
    private JTextField tfSurname;
    private JTextField tfBank;
    private JComboBox cbManufacturer;

    private List<Manufacturer> manufacturerList;
    private Worker worker;

    public NewWorker() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);


        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Manufacturer");
        this.manufacturerList = query.list();

        cbManufacturer.removeAllItems();
        for(Manufacturer manufacturer : manufacturerList){
            cbManufacturer.addItem(manufacturer);
        }

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newWorker();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    private void newWorker(){
        if(!tfName.getText().equalsIgnoreCase("") || !tfDni.getText().equalsIgnoreCase("") || !tfSurname.getText().equalsIgnoreCase("")){
            if(this.worker==null)
                this.worker = new Worker();

            this.worker.setDni(tfDni.getText());
            this.worker.setName(tfName.getText());
            this.worker.setSurname(tfSurname.getText());
            this.worker.setBankAccount(tfBank.getText());
            this.worker.setManufacturer(this.manufacturerList.get(cbManufacturer.getSelectedIndex()));

            setVisible(false);
        }else{
            JOptionPane.showMessageDialog(this, "You must fill in the obligatory fields");
        }
    }

    public void prepareEdit(int id_worker){
        buttonOK.setText("Edit");
        this.worker = (Worker) HibernateUtil.getCurrentSession().get(Worker.class, id_worker);

        tfDni.setText(worker.getDni());
        tfName.setText(worker.getName());
        tfSurname.setText(worker.getSurname());
        tfBank.setText(worker.getBankAccount());
    }

    public Worker getWorker() {
        return worker;
    }

    public static void main(String[] args) {
        NewWorker dialog = new NewWorker();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
