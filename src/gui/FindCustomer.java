package gui;

import data.Computer;
import data.Customer;
import util.HibernateUtil;

import javax.management.Query;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.List;

public class FindCustomer extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTextField tfSearch;
    private JList lstCustomers;
    private JList lstComputer;
    private JScrollPane scrollCustomers;
    private JScrollPane scrollComputers;
    private JButton btSearch;

    private List<Customer> customerList;
    private DefaultListModel customerListModel;
    private DefaultListModel computerListModel;

    public FindCustomer() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(btSearch);

        this.customerListModel = new DefaultListModel();
        this.computerListModel = new DefaultListModel();
        lstCustomers.setModel(customerListModel);
        lstComputer.setModel(computerListModel);

        org.hibernate.Query query = HibernateUtil.getCurrentSession().createQuery("FROM Customer");
        this.customerList = query.list();

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        btSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                search();
            }
        });
    }

    private void search(){
        Boolean hasComputer = false;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        if(!tfSearch.getText().equalsIgnoreCase("")){
            customerListModel.removeAllElements();
            computerListModel.removeAllElements();

            String searchString = tfSearch.getText();
            for(Customer customer : customerList){
                for(Computer computer : customer.getComputers()){
                    if(computer.getCode().contains(searchString) || computer.getName().contains(searchString)){
                        computerListModel.addElement(computer);
                        hasComputer = true;
                    }
                }
                if(hasComputer){
                    customerListModel.addElement(customer);
                    hasComputer = false;
                    continue;
                }

                if(customer.getName().contains(searchString) || customer.getSurname().contains(searchString)
                        || customer.getAdress().contains(searchString) || customer.getMail().contains(searchString)
                        || customer.getTelephone().contains(searchString)){
                    customerListModel.addElement(customer);
                }
            }
        }else{
            JOptionPane.showMessageDialog(null, "You must fill in the search bar first");
        }
    }

    public static void main(String[] args) {
        FindCustomer dialog = new FindCustomer();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
