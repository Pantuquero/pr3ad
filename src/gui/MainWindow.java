package gui;

import data.*;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.service.ServiceRegistry;
import threads.Refresher;
import util.HibernateUtil;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MainWindow {
    private JPanel panel1;
    private JPanel StateBarPanel;
    private JPanel MainPanel;
    public JLabel StateBar;
    private JButton btNew;
    private JScrollPane MainScrollPane;
    private JPanel TablePanel;
    private JTable tbCustomers;
    private JTable tbComputers;
    private JTable tbManufacturers;
    private JTable tbDeliverers;
    private JTable tbWorkers;
    private JButton btDeleteCustomer;
    private JButton btEditCustomer;
    private JButton btNewCustomer;
    private JButton btNewComputer;
    private JButton btEditComputer;
    private JButton btDeleteComputer;
    private JButton btNewManufacturer;
    private JButton btEditManufacturer;
    private JButton btDeleteManufacturer;
    private JButton btNewDeliverer;
    private JButton btEditDeliverer;
    private JButton btDeleteDeliverer;
    private JButton btNewWorker;
    private JButton btEditWorker;
    private JButton btDeleteWorker;
    private JScrollPane CustomerScroll;
    private JScrollPane ComputerScroll;
    private JScrollPane ManufacturerScroll;
    private JScrollPane DelivererScroll;
    private JScrollPane WorkerScroll;
    private JButton btFindCustomer;

    private DefaultTableModel customerTableModel;
    private DefaultTableModel manufacturerTableModel;
    private DefaultTableModel workerTableModel;
    private DefaultTableModel delivererTableModel;
    private DefaultTableModel computerTableModel;

    private Refresher refresher;

    public MainWindow(){
        connect();

        LogIn logIn = new LogIn();
        logIn.setSize(188, 130);
        logIn.setVisible(true);

        buildTables();
        loadAllTableData();

        btNewCustomer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newCustomer();
            }
        });
        btEditCustomer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editCustomer();
            }
        });
        btDeleteCustomer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteCustomer();
            }
        });

        btNewManufacturer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newManufacturer();
            }
        });
        btEditManufacturer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editManufacturer();
            }
        });
        btDeleteManufacturer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteManufacturer();
            }
        });

        btNewWorker.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newWorker();
            }
        });
        btEditWorker.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editWorker();
            }
        });
        btDeleteWorker.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteWorker();
            }
        });

        btNewDeliverer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newDeliverer();
            }
        });
        btEditDeliverer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editDeliverer();
            }
        });
        btDeleteDeliverer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteDeliverer();
            }
        });

        btNewComputer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newComputer();
            }
        });
        btEditComputer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editComputer();
            }
        });
        btDeleteComputer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteComputer();
            }
        });

        btFindCustomer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findCustomer();
            }
        });

        this.refresher = new Refresher(this);
        refresher.start();
    }

    private void findCustomer(){
        FindCustomer findCustomer = new FindCustomer();
        findCustomer.setSize(525, 259);
        findCustomer.setVisible(true);
    }

    private void newCustomer(){
        NewCustomer newCustomer = new NewCustomer();
        newCustomer.setSize(194, 244);
        newCustomer.setVisible(true);

        if(newCustomer.getCustomer()!=null){
            Session session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.save(newCustomer.getCustomer());
            session.getTransaction().commit();
            session.close();

            loadCustomerTableData();
            this.StateBar.setText("New customer created successfully");
        }else{
            this.StateBar.setText("New customer creation cancelled");
        }
    }

    private void editCustomer(){
        int row = tbCustomers.getSelectedRow();
        if(row !=-1){
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String date = String.valueOf(tbCustomers.getValueAt(row, 3));
            java.util.Date javaDate = new java.util.Date();

            try {
                javaDate = formatter.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            java.sql.Date sqlDate = new java.sql.Date(javaDate.getTime());

            int id = (Integer) tbCustomers.getValueAt(row, 0);
            Customer customer = (Customer) HibernateUtil.getCurrentSession().get(Customer.class, id);

            customer.setName(String.valueOf(tbCustomers.getValueAt(row, 1)));
            customer.setSurname(String.valueOf(tbCustomers.getValueAt(row, 2)));
            customer.setBirth(sqlDate);
            customer.setAdress(String.valueOf(tbCustomers.getValueAt(row, 4)));
            customer.setMail(String.valueOf(tbCustomers.getValueAt(row, 5)));
            customer.setTelephone(String.valueOf(tbCustomers.getValueAt(row, 6)));

            Session session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.update(customer);
            session.getTransaction().commit();
            session.close();

            loadCustomerTableData();
            this.StateBar.setText("Customer modified successfully");
        }else{
            JOptionPane.showMessageDialog(null, "Select one row to edit");
        }
    }

    private void deleteCustomer(){
        int row = tbCustomers.getSelectedRow();
        if(row!=-1){
            int id = (Integer) tbCustomers.getValueAt(row, 0);
            Customer customer = (Customer) HibernateUtil.getCurrentSession().get(Customer.class, id);

            Session session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.delete(customer);
            session.getTransaction().commit();
            session.close();

            loadCustomerTableData();
            this.StateBar.setText("Customer deleted successfully");
        }else{
            JOptionPane.showMessageDialog(null, "Select row to delete");
        }
    }

    private void newManufacturer(){
        NewManufacturer newManufacturer = new NewManufacturer();
        newManufacturer.setSize(300, 500);
        newManufacturer.setVisible(true);

        if(newManufacturer.getManufacturer()!=null){
            Session session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.save(newManufacturer.getManufacturer());
            session.getTransaction().commit();
            session.close();

            loadManufacturerTableData();
            this.StateBar.setText("New manufacturer created successfully");
        }else{
            this.StateBar.setText("New manufacturer creation cancelled");
        }
    }

    private void editManufacturer(){
        int row = tbManufacturers.getSelectedRow();
        if(row !=-1) {
            NewManufacturer newManufacturer = new NewManufacturer();
            newManufacturer.setSize(300, 500);
            newManufacturer.prepareEdit((Integer) tbManufacturers.getValueAt(row, 0));
            newManufacturer.setVisible(true);

            if (newManufacturer.getManufacturer() != null) {
                Session session = HibernateUtil.getCurrentSession();
                session.beginTransaction();
                session.update(newManufacturer.getManufacturer());
                session.getTransaction().commit();
                session.close();

                loadManufacturerTableData();
                this.StateBar.setText("Manufacturer edited successfully");
            } else {
                this.StateBar.setText("Manufacturer edit cancellled");
            }
        }else{
            JOptionPane.showMessageDialog(null, "Select one row to edit");
        }
    }

    private void deleteManufacturer(){
        int row = tbManufacturers.getSelectedRow();
        if(row!=-1){
            int id = (Integer) tbManufacturers.getValueAt(row, 0);
            Manufacturer manufacturer = (Manufacturer) HibernateUtil.getCurrentSession().get(Manufacturer.class, id);

            Session session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.delete(manufacturer);
            session.getTransaction().commit();
            session.close();

            loadManufacturerTableData();
            this.StateBar.setText("Manufacturer deleted successfully");
        }else{
            JOptionPane.showMessageDialog(null, "Select row to delete");
        }
    }

    private void newWorker(){
        NewWorker newWorker = new NewWorker();
        newWorker.setSize(194, 244);
        newWorker.setVisible(true);

        if(newWorker.getWorker()!=null){
            Session session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.save(newWorker.getWorker());
            session.getTransaction().commit();
            session.close();

            loadWorkerTableData();
            this.StateBar.setText("New worker created successfully");
        }else{
            this.StateBar.setText("New worker creation cancellled");
        }
    }

    private void editWorker(){
        int row = tbWorkers.getSelectedRow();
        if(row !=-1) {
            NewWorker newWorker = new NewWorker();
            newWorker.setSize(194, 244);
            newWorker.prepareEdit((Integer) tbWorkers.getValueAt(row, 0));
            newWorker.setVisible(true);

            if (newWorker.getWorker() != null) {
                Session session = HibernateUtil.getCurrentSession();
                session.beginTransaction();
                session.update(newWorker.getWorker());
                session.getTransaction().commit();
                session.close();

                loadWorkerTableData();
                this.StateBar.setText("Worker edited successfully");
            } else {
                this.StateBar.setText("Worker edit cancellled");
            }
        }else{
            JOptionPane.showMessageDialog(null, "Select one row to edit");
        }
    }

    private void deleteWorker(){
        int row = tbWorkers.getSelectedRow();
        if(row!=-1){
            int id = (Integer) tbWorkers.getValueAt(row, 0);
            Worker worker = (Worker) HibernateUtil.getCurrentSession().get(Worker.class, id);

            Session session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.delete(worker);
            session.getTransaction().commit();
            session.close();

            loadWorkerTableData();
            this.StateBar.setText("Worker deleted successfully");
        }else{
            JOptionPane.showMessageDialog(null, "Select row to delete");
        }
    }

    private void newDeliverer(){
        NewDeliverer newDeliverer = new NewDeliverer();
        newDeliverer.setSize(300, 500);
        newDeliverer.setVisible(true);

        if(newDeliverer.getDeliverer()!=null){
            Session session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.save(newDeliverer.getDeliverer());
            session.getTransaction().commit();
            session.close();

            loadDelivererTableData();
            this.StateBar.setText("New deliverer created successfully");
        }else{
            this.StateBar.setText("New deliverer creation cancellled");
        }
    }

    private void editDeliverer(){
        int row = tbDeliverers.getSelectedRow();
        if(row !=-1) {
            NewDeliverer newDeliverer = new NewDeliverer();
            newDeliverer.setSize(300, 500);
            newDeliverer.prepareEdit((Integer) tbDeliverers.getValueAt(row, 0));
            newDeliverer.setVisible(true);

            if (newDeliverer.getDeliverer() != null) {
                Session session = HibernateUtil.getCurrentSession();
                session.beginTransaction();
                session.update(newDeliverer.getDeliverer());
                session.getTransaction().commit();
                session.close();

                loadDelivererTableData();
                this.StateBar.setText("Deliverer edited successfully");
            } else {
                this.StateBar.setText("Deliverer edit cancellled");
            }
        }else{
            JOptionPane.showMessageDialog(null, "Select one row to edit");
        }
    }

    private void deleteDeliverer(){
        int row = tbDeliverers.getSelectedRow();
        if(row!=-1){
            int id = (Integer) tbDeliverers.getValueAt(row, 0);
            Deliverer deliverer= (Deliverer) HibernateUtil.getCurrentSession().get(Deliverer.class, id);

            Session session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.delete(deliverer);
            session.getTransaction().commit();
            session.close();

            loadDelivererTableData();
            this.StateBar.setText("Deliverer deleted successfully");
        }else{
            JOptionPane.showMessageDialog(null, "Select row to delete");
        }
    }

    private void newComputer(){
        NewComputer newComputer= new NewComputer();
        newComputer.setSize(194, 244);
        newComputer.setVisible(true);

        if(newComputer.getComputer()!=null){
            Session session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.save(newComputer.getComputer());
            session.getTransaction().commit();
            session.close();

            loadComputerTableData();
            this.StateBar.setText("New computer created successfully");
        }else{
            this.StateBar.setText("New computer creation cancellled");
        }
    }

    private void editComputer(){
        int row = tbComputers.getSelectedRow();
        if(row !=-1) {
            NewComputer newComputer = new NewComputer();
            newComputer.setSize(194, 244);
            newComputer.prepareEdit((Integer) tbComputers.getValueAt(row, 0));
            newComputer.setVisible(true);

            if (newComputer.getComputer() != null) {
                Session session = HibernateUtil.getCurrentSession();
                session.beginTransaction();
                session.update(newComputer.getComputer());
                session.getTransaction().commit();
                session.close();

                loadComputerTableData();
                this.StateBar.setText("Computer edited successfully");
            } else {
                this.StateBar.setText("Computer edit cancellled");
            }
        }else{
            JOptionPane.showMessageDialog(null, "Select one row to edit");
        }
    }

    private void deleteComputer(){
        int row = tbComputers.getSelectedRow();
        if(row!=-1){
            int id = (Integer) tbComputers.getValueAt(row, 0);
            Computer computer = (Computer) HibernateUtil.getCurrentSession().get(Computer.class, id);

            Session session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.delete(computer);
            session.getTransaction().commit();
            session.close();

            loadComputerTableData();
            this.StateBar.setText("Computer deleted successfully");
        }else{
            JOptionPane.showMessageDialog(null, "Select row to delete");
        }
    }

    public void buildTables(){
        this.customerTableModel = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 0) {
                    return false;
                }else {
                    return true;
                }
            }
        };
        this.manufacturerTableModel  = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 0) {
                    return false;
                }else {
                    return true;
                }
            }
        };
        this.workerTableModel = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 0) {
                    return false;
                }else {
                    return true;
                }
            }
        };
        this.delivererTableModel = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 0) {
                    return false;
                }else {
                    return true;
                }
            }
        };
        this.computerTableModel = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 0) {
                    return false;
                }else {
                    return true;
                }
            }
        };

        this.customerTableModel.addColumn("#");
        this.customerTableModel.addColumn("Name");
        this.customerTableModel.addColumn("Surname");
        this.customerTableModel.addColumn("Birth date");
        this.customerTableModel.addColumn("Adress");
        this.customerTableModel.addColumn("E-mail");
        this.customerTableModel.addColumn("Telephone");

        this.manufacturerTableModel.addColumn("#");
        this.manufacturerTableModel.addColumn("Name");
        this.manufacturerTableModel.addColumn("Code");
        this.manufacturerTableModel.addColumn("Adress");
        this.manufacturerTableModel.addColumn("E-mail");
        this.manufacturerTableModel.addColumn("Telephone");

        this.workerTableModel.addColumn("#");
        this.workerTableModel.addColumn("DNI");
        this.workerTableModel.addColumn("Name");
        this.workerTableModel.addColumn("Surname");
        this.workerTableModel.addColumn("Bank account");

        this.delivererTableModel.addColumn("#");
        this.delivererTableModel.addColumn("Name");
        this.delivererTableModel.addColumn("Code");
        this.delivererTableModel.addColumn("Adress");
        this.delivererTableModel.addColumn("E-mail");
        this.delivererTableModel.addColumn("Telephone");

        this.computerTableModel.addColumn("#");
        this.computerTableModel.addColumn("Code");
        this.computerTableModel.addColumn("Name");
        this.computerTableModel.addColumn("Description");
        this.computerTableModel.addColumn("Price");

        this.tbCustomers.setModel(customerTableModel);
        this.tbManufacturers.setModel(manufacturerTableModel);
        this.tbWorkers.setModel(workerTableModel);
        this.tbDeliverers.setModel(delivererTableModel);
        this.tbComputers.setModel(computerTableModel);

        this.StateBar.setText("Tables built succesfully");
    }

    private void connect(){
        try{
            HibernateUtil.buildSessionFactory();
            HibernateUtil.openSession();

            this.StateBar.setText("Connection succesful with database");
        }catch (HibernateException he){
            he.printStackTrace();
            this.StateBar.setText("Error while connecting with database");
        }

    }

    public void loadAllTableData(){
        loadCustomerTableData();
        loadManufacturerTableData();
        loadWorkerTableData();
        loadDelivererTableData();
        loadComputerTableData();

        this.StateBar.setText("All table data loaded successfully");
    }

    public void loadCustomerTableData(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Customer");
        List<Customer> customerList = query.list();

        this.customerTableModel.setNumRows(0);
        for(Customer customer : customerList){
            Object[] row = new Object[]{
                    customer.getId(),
                    customer.getName(),
                    customer.getSurname(),
                    customer.getBirth(),
                    customer.getAdress(),
                    customer.getMail(),
                    customer.getTelephone()
            };
            this.customerTableModel.addRow(row);
        }

        this.StateBar.setText("Customer table loaded successfully");
    }

    public void loadManufacturerTableData(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Manufacturer");
        List<Manufacturer> manufacturerList = query.list();

        this.manufacturerTableModel.setNumRows(0);
        for(Manufacturer manufacturer : manufacturerList){
            Object[] row = new Object[]{
                    manufacturer.getId(),
                    manufacturer.getName(),
                    manufacturer.getCode(),
                    manufacturer.getAdress(),
                    manufacturer.getMail(),
                    manufacturer.getTelephone()
            };
            this.manufacturerTableModel.addRow(row);
        }

        this.StateBar.setText("Manufacturer table loaded successfully");
    }

    public void loadWorkerTableData(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Worker");
        List<Worker> workerList = query.list();

        this.workerTableModel.setNumRows(0);
        for(Worker worker : workerList){
            Object[] row = new Object[]{
                    worker.getId(),
                    worker.getDni(),
                    worker.getName(),
                    worker.getSurname(),
                    worker.getBankAccount(),
            };
            this.workerTableModel.addRow(row);
        }

        this.StateBar.setText("Worker table loaded successfully");
    }

    public void loadDelivererTableData(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Deliverer");
        List<Deliverer> delivererList = query.list();

        this.delivererTableModel.setNumRows(0);
        for(Deliverer deliverer : delivererList){
            Object[] row = new Object[]{
                    deliverer.getId(),
                    deliverer.getName(),
                    deliverer.getCode(),
                    deliverer.getAdress(),
                    deliverer.getMail(),
                    deliverer.getTelephone()
            };
            this.delivererTableModel.addRow(row);
        }

        this.StateBar.setText("Deliverer table loaded successfully");
    }

    public void loadComputerTableData(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Computer");
        List<Computer> computerList = query.list();

        this.computerTableModel.setNumRows(0);
        for(Computer computer : computerList){
            Object[] row = new Object[]{
                    computer.getId(),
                    computer.getCode(),
                    computer.getName(),
                    computer.getDescription(),
                    computer.getPrice()
            };
            this.computerTableModel.addRow(row);
        }

        this.StateBar.setText("Computer table loaded successfully");
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Práctica 3");
        frame.setContentPane(new MainWindow().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
