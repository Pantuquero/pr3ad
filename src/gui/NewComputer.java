package gui;

import data.Computer;
import data.Customer;
import data.Manufacturer;
import org.hibernate.Query;
import util.HibernateUtil;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class NewComputer extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfCode;
    private JTextField tfName;
    private JTextField tfDescription;
    private JTextField tfPrice;
    private JComboBox cbCustomer;
    private JComboBox cbManufacturer;

    private Computer computer;
    private List<Customer> customerList;
    private List<Manufacturer> manufacturerList;

    public NewComputer() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        loadCombos();

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newComputer();
            }
        });
        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    private void newComputer(){
        if(!tfName.getText().equalsIgnoreCase("") || !tfCode.getText().equalsIgnoreCase("")){
            if(this.computer==null)
                this.computer = new Computer();

            this.computer.setCode(tfCode.getText());
            this.computer.setName(tfName.getText());
            this.computer.setDescription(tfDescription.getText());
            this.computer.setPrice(Float.valueOf(tfPrice.getText()));
            this.computer.setCustomer(this.customerList.get(cbCustomer.getSelectedIndex()));
            this.computer.setManufacturer(this.manufacturerList.get(cbManufacturer.getSelectedIndex()));

            setVisible(false);
        }else{
            JOptionPane.showMessageDialog(this, "You must fill in the obligatory fields");
        }
    }

    public void prepareEdit(int id_computer){
        buttonOK.setText("Edit");
        this.computer = (Computer) HibernateUtil.getCurrentSession().get(Computer.class, id_computer);

        tfCode.setText(computer.getCode());
        tfName.setText(computer.getName());
        tfDescription.setText(computer.getDescription());
        tfPrice.setText(computer.getPrice().toString());
    }

    private void loadCombos(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Customer");
        this.customerList = query.list();

        cbCustomer.removeAllItems();
        for(Customer customer : customerList)
            cbCustomer.addItem(customer);

        query = HibernateUtil.getCurrentSession().createQuery("FROM Manufacturer");
        this.manufacturerList = query.list();

        cbManufacturer.removeAllItems();
        for(Manufacturer manufacturer : manufacturerList)
            cbManufacturer.addItem(manufacturer);
    }

    public Computer getComputer() {
        return computer;
    }

    public static void main(String[] args) {
        NewComputer dialog = new NewComputer();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
