package gui;

import com.sun.org.apache.xpath.internal.operations.Bool;
import data.User;
import org.hibernate.Query;
import util.HibernateUtil;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class LogIn extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfPassword;
    private JTextField tfUser;

    private List<User> users;

    public LogIn() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logIn();
            }
        });
        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
    }

    private void logIn(){
        Boolean correct = false;
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM User");
        this.users = query.list();

        for(User user : users){
            if(user.getName().equalsIgnoreCase(tfUser.getText()) && user.getPass().equalsIgnoreCase(tfPassword.getText())){
                dispose();
                correct = true;
            }
        }
        if(!correct)
            JOptionPane.showMessageDialog(null, "Incorrect username/password");
    }

    public static void main(String[] args) {
        LogIn dialog = new LogIn();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
