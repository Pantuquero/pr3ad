package gui;

import data.Deliverer;
import data.Manufacturer;
import org.hibernate.Query;
import util.HibernateUtil;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class NewDeliverer extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfName;
    private JTextField tfCode;
    private JTextField tfAdress;
    private JTextField tfMail;
    private JTextField tfTelephone;
    private JList lstManufacturers;
    private JScrollPane scrollManufacturers;

    private Deliverer deliverer;
    private List<Manufacturer> manufacturerList;
    private DefaultListModel manufacturerListModel;

    public NewDeliverer() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        loadManufacturerList();

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newDeliverer();
            }
        });
        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

     private void newDeliverer(){
         if(!tfName.getText().equalsIgnoreCase("") || !tfCode.getText().equalsIgnoreCase("") || !tfAdress.getText().equalsIgnoreCase("") || !tfMail.getText().equalsIgnoreCase("") || !tfTelephone.getText().equalsIgnoreCase("")){
             this.deliverer = new Deliverer();

             this.deliverer.setName(tfName.getText());
             this.deliverer.setCode(tfCode.getText());
             this.deliverer.setAdress(tfAdress.getText());
             this.deliverer.setMail(tfMail.getText());
             this.deliverer.setTelephone(tfTelephone.getText());

             List<Manufacturer> manufacturerListDirty = lstManufacturers.getSelectedValuesList();
             this.deliverer.setManufacturers(manufacturerListDirty);

             for(int i=0;i<manufacturerListDirty.size();i++){
                 manufacturerListDirty.get(i).getDeliverers().add(deliverer);
             }

             setVisible(false);
         }else{
             JOptionPane.showMessageDialog(this, "You must fill in the obligatory fields");
         }
     }

    public void prepareEdit(int id_deliverer){
        buttonOK.setText("Edit");
        this.deliverer = (Deliverer) HibernateUtil.getCurrentSession().get(Deliverer.class, id_deliverer);

        tfName.setText(deliverer.getName());
        tfCode.setText(deliverer.getCode());
        tfAdress.setText(deliverer.getAdress());
        tfMail.setText(deliverer.getMail());
        tfTelephone.setText(deliverer.getTelephone());
    }

    private void loadManufacturerList(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Manufacturer");
        this.manufacturerList = query.list();

        this.manufacturerListModel = new DefaultListModel();
        this.manufacturerListModel.removeAllElements();
        for(Manufacturer manufacturer : manufacturerList)
            this.manufacturerListModel.addElement(manufacturer);

        this.lstManufacturers.setModel(manufacturerListModel);
    }

    public Deliverer getDeliverer() {
        return deliverer;
    }

    public static void main(String[] args) {
        NewDeliverer dialog = new NewDeliverer();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
