package data;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Pantuquero on 10/02/2015.
 */
@Entity
@Table(name = "deliverers", schema = "", catalog = "pr3ad")
public class Deliverer {
    private int id;
    private String name;
    private String code;
    private String adress;
    private String mail;
    private String telephone;
    private List<Manufacturer> manufacturers;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "adress")
    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    @Basic
    @Column(name = "mail")
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Basic
    @Column(name = "telephone")
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Deliverer deliverer = (Deliverer) o;

        if (id != deliverer.id) return false;
        if (adress != null ? !adress.equals(deliverer.adress) : deliverer.adress != null) return false;
        if (code != null ? !code.equals(deliverer.code) : deliverer.code != null) return false;
        if (mail != null ? !mail.equals(deliverer.mail) : deliverer.mail != null) return false;
        if (name != null ? !name.equals(deliverer.name) : deliverer.name != null) return false;
        if (telephone != null ? !telephone.equals(deliverer.telephone) : deliverer.telephone != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (adress != null ? adress.hashCode() : 0);
        result = 31 * result + (mail != null ? mail.hashCode() : 0);
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        return result;
    }

    @ManyToMany(mappedBy = "deliverers")
    public List<Manufacturer> getManufacturers() {
        return manufacturers;
    }

    public void setManufacturers(List<Manufacturer> deliverers) {
        this.manufacturers = deliverers;
    }

    public String toString(){
        return this.name;
    }
}
