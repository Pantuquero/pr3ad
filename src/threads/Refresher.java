package threads;


import data.Customer;
import gui.MainWindow;
import org.hibernate.Query;
import util.HibernateUtil;

import java.util.List;

public class Refresher extends Thread{
    private MainWindow mainWindow;

    public Refresher(MainWindow mainWindow){
        this.mainWindow = mainWindow;
    }

    @Override
    public void run() {
        while (HibernateUtil.getCurrentSession().isOpen()){
            try {
                Thread.sleep(5000);
                mainWindow.loadAllTableData();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
